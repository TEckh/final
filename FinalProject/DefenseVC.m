//
//  DefenseVC.m
//  FinalProject
//
//  Created by Trevor Eckhardt on 7/28/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

//App crashes when it tries to load this view.

#import "DefenseVC.h"
#import <Parse/Parse.h>
#import "LoginVC.h"

@interface DefenseVC ()
@property (weak, nonatomic) IBOutlet UITextField *HP;
@property (weak, nonatomic) IBOutlet UITextField *HPCurrent;
@property (weak, nonatomic) IBOutlet UITextField *HPTemp;
@property (weak, nonatomic) IBOutlet UILabel *AC;
@property (weak, nonatomic) IBOutlet UILabel *DeathSucc;
@property (weak, nonatomic) IBOutlet UILabel *DeathFail;
@property (weak, nonatomic) IBOutlet UIStepper *DeathSuccStep;
@property (weak, nonatomic) IBOutlet UIStepper *DeathFailStep;
@property (weak, nonatomic) IBOutlet UILabel *STRsave;
@property (weak, nonatomic) IBOutlet UILabel *DEXsave;
@property (weak, nonatomic) IBOutlet UILabel *CONsave;
@property (weak, nonatomic) IBOutlet UILabel *INTsave;
@property (weak, nonatomic) IBOutlet UILabel *WISsave;
@property (weak, nonatomic) IBOutlet UILabel *CHAsave;

@end

@implementation DefenseVC

LoginVC* lvc;

//Can't quite figure out how to make the values change.
- (IBAction)FailValueChanged:(id)sender
{
    //self.DeathFailStep = (UIStepper*)sender;
    
    self.DeathFail.text = [NSString stringWithFormat:@"%.f", self.DeathFailStep.value];
}

- (IBAction)SuccessValueChanged:(id)sender
{
    //self.DeathSuccStep = (UIStepper*)sender;
    
    self.DeathSucc.text = [NSString stringWithFormat:@"%.f", self.DeathSuccStep.value];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor brownColor];
    
    //Obscures the entire view. I only see the background.
//    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Scroll2.png"]];
//    [self.view addSubview:background];
    
//    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"Character" ofType:@"plist"];
//    NSArray* array = [[NSArray alloc] initWithContentsOfFile:filePath];
//    
//    NSMutableDictionary* Defense = [array objectAtIndex:1];
//    NSMutableDictionary* Stats = [array objectAtIndex:5];
    
    self.AC.text = [lvc.character objectForKey:@"ArmorClass"];
    self.STRsave.text = [lvc.character objectForKey:@"STRmod"];
    self.DEXsave.text = [lvc.character objectForKey:@"DEXmod"];
    self.CONsave.text = [lvc.character objectForKey:@"CONmod"];
    self.INTsave.text = [lvc.character objectForKey:@"INTmod"];
    self.WISsave.text = [lvc.character objectForKey:@"WISmod"];
    self.CHAsave.text = [lvc.character objectForKey:@"CHAmod"];
    self.HP.text = [lvc.character objectForKey:@"HitPoints"];
    self.HPCurrent.text = [lvc.character objectForKey:@"HPCurrent"];
    self.HPTemp.text = [lvc.character objectForKey:@"HPTemporary"];
    
    // Do any additional setup after loading the view.
}
- (IBAction)HPEditEnd:(UITextField *)sender {
}
- (IBAction)HPCurrEditEnd:(UITextField *)sender {
}
- (IBAction)HPTempEditEnd:(UITextField *)sender {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
