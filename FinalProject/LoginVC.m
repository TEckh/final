//
//  LoginVC.m
//  FinalProject
//
//  Created by Trevor Eckhardt on 8/14/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "LoginVC.h"
#import <Parse/Parse.h>

@interface LoginVC ()

@end

@implementation LoginVC

UITextField* charName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor brownColor];
    
    UILabel* alert = [[UILabel alloc] initWithFrame:CGRectMake(309, 400, 200, 50)];
    alert.text = @"If you have created a previous character:";
    alert.adjustsFontSizeToFitWidth = YES;
    alert.textAlignment = NSTextAlignmentCenter;
    
    charName = [[UITextField alloc] initWithFrame:CGRectMake(309, 500, 150, 50)];
    charName.borderStyle = UITextBorderStyleRoundedRect;
    charName.placeholder = @"Enter Character name here";
    [charName setFont:[UIFont boldSystemFontOfSize:17]];
    [self.view addSubview:charName];
    
    UIButton* retrieveButton;
    [retrieveButton setTitle:@"Retrieve Character" forState:UIControlStateNormal];
    [retrieveButton sizeToFit];
    retrieveButton.center = CGPointMake(768/2, 600);
    
    UIButton* saveButton;
    [saveButton setTitle:@"Save Character" forState:UIControlStateNormal];
    [saveButton sizeToFit];
    saveButton.center = CGPointMake(768/2, 700);
    
    [self.view addSubview:retrieveButton];
    // Do any additional setup after loading the view.
}

-(void) retrieveButtonPressed:(UIButton*) button
{
    PFQuery *query = [PFQuery queryWithClassName:@"Characters"];
    [query getObjectInBackgroundWithId:charName.text block:^(PFObject* character, NSError* error)
     {
         if(error)
             NSLog(@"Unable to retrieve data.");
         else
             self.character = character;
     }];
}

-(void) saveButtonPressed:(UIButton*) button
{
    [self.character saveInBackgroundWithBlock:^(BOOL succeeded, NSError* error){
        if(succeeded)
        {
            //Character saved. Inform user.
        }
        else
        {
            //Character didn't save. Alert user.
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
