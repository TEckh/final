//
//  LoginVC.h
//  FinalProject
//
//  Created by Trevor Eckhardt on 8/14/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface LoginVC : UIViewController

@property(nonatomic, strong) PFObject* character;

@end
