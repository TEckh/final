//
//  TreasureVC.m
//  FinalProject
//
//  Created by Trevor Eckhardt on 7/28/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "TreasureVC.h"
#import <Parse/Parse.h>
#import "LoginVC.h"

@interface TreasureVC ()
@property (weak, nonatomic) IBOutlet UITextField *CP;
@property (weak, nonatomic) IBOutlet UITextField *SP;
@property (weak, nonatomic) IBOutlet UITextField *GP;
@property (weak, nonatomic) IBOutlet UITextField *PP;
@property (weak, nonatomic) IBOutlet UITextView *MiscTreasure;
@property (weak, nonatomic) IBOutlet UITextView *Equip;
@property (weak, nonatomic) IBOutlet UITextView *Allies;


@end

@implementation TreasureVC

LoginVC* lvc;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor brownColor];
    
    //Same issue here too.
//    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Scroll2.png"]];
//    [self.view addSubview:background];

//    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"Character" ofType:@"plist"];
//    NSArray* array = [[NSArray alloc] initWithContentsOfFile:filePath];
//    
//    NSMutableDictionary* Treasure = [array objectAtIndex:0];

    NSError *error = nil;
    
    self.MiscTreasure.text = [lvc.character objectForKey:@"MiscTreasure"];
    self.Allies.text = [lvc.character objectForKey:@"Allies"];
    self.Equip.text = [lvc.character objectForKey:@"Equip"];
    self.CP.text = [lvc.character objectForKey:@"CP"];
    self.SP.text = [lvc.character objectForKey:@"SP"];
    self.GP.text = [lvc.character objectForKey:@"GP"];
    self.PP.text = [lvc.character objectForKey:@"PP"];
    
    NSString *miscTreas = self.MiscTreasure.text;
    NSString* equip = self.Equip.text;
    NSString* allies = self.Allies.text;
    
    //Is this the correct way of changing the values for my plist? Not sure.
    if (![miscTreas writeToFile:[lvc.character objectForKey:@"MiscTreasure"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![equip writeToFile:[lvc.character objectForKey:@"Equip"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![allies writeToFile:[lvc.character objectForKey:@"Allies"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    
    // Do any additional setup after loading the view.
}
- (IBAction)CPEditEnd:(UITextField *)sender {
}
- (IBAction)SPEditEnd:(UITextField *)sender {
}
- (IBAction)GPEditEnd:(UITextField *)sender {
}
- (IBAction)PPEditEnd:(UITextField *)sender {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
