//
//  SecondViewController.m
//  FinalProject
//
//  Created by Trevor Eckhardt on 7/28/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

//App crashes when it tries to load this view.

#import "AttackVC.h"
#import <Parse/Parse.h>
#import "LoginVC.h"

@interface AttackVC ()
@property (weak, nonatomic) IBOutlet UILabel *InitBonus;
@property (weak, nonatomic) IBOutlet UILabel *SpellAtt;
@property (weak, nonatomic) IBOutlet UILabel *SpellDC;
@property (weak, nonatomic) IBOutlet UITextField *Speed;
@property (weak, nonatomic) IBOutlet UITextField *Wp1NameType;
@property (weak, nonatomic) IBOutlet UITextField *Wp2NameType;
@property (weak, nonatomic) IBOutlet UITextField *Wp3NameType;
@property (weak, nonatomic) IBOutlet UILabel *Wp1Att;
@property (weak, nonatomic) IBOutlet UILabel *Wp2Att;
@property (weak, nonatomic) IBOutlet UILabel *Wp3Att;
@property (weak, nonatomic) IBOutlet UITextField *Wp1Dam;
@property (weak, nonatomic) IBOutlet UITextField *Wp2Dam;
@property (weak, nonatomic) IBOutlet UITextField *Wp3Dam;

@end

@implementation AttackVC

LoginVC* lvc;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // This one works, it's just the wrong size.
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Scroll2.png"]];
    [self.view addSubview:background];
    
    int attack, DC;
    
//    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"Character" ofType:@"plist"];
//    NSArray* array = [[NSArray alloc] initWithContentsOfFile:filePath];
//    
//    NSMutableDictionary* Attacks = [array objectAtIndex:3];
//    NSMutableDictionary* CharInform = [array objectAtIndex:4];
//    NSMutableDictionary* Stats = [array objectAtIndex:5];
//    NSMutableDictionary* Weapon1 = [Attacks objectForKey:@"WP1"];
//    NSMutableDictionary* Weapon2 = [Attacks objectForKey:@"WP2"];
//    NSMutableDictionary* Weapon3 = [Attacks objectForKey:@"WP3"];
    
    NSString* Race = [lvc.character objectForKey:@"Race"];
    NSString* Class = [lvc.character objectForKey:@"Class"];
    int cha = [lvc.character objectForKey:@"CHAmod"];
    int wis = [lvc.character objectForKey:@"WISmod"];
    int intel = [lvc.character objectForKey:@"INTmod"];
    int prof = [lvc.character objectForKey:@"ProfBonus"];
    
    //set Fields
    self.Wp1NameType.text = [lvc.character objectForKey:@"Weapon1NameType"];
    self.Wp1Dam.text = [lvc.character objectForKey:@"Weapon1Damage"];
    self.Wp2NameType.text = [lvc.character objectForKey:@"Weapon2NameType"];
    self.Wp2Dam.text = [lvc.character objectForKey:@"Weapon2Damage"];
    self.Wp3NameType.text = [lvc.character objectForKey:@"Weapon3NameType"];
    self.Wp3Dam.text = [lvc.character objectForKey:@"Weapon3Damage"];
    self.InitBonus.text = [lvc.character objectForKey:@"DEXmod"];
    
    if([Race isEqual:@"Dwarf"] || [Race isEqual:@"Gnome"] || [Race isEqual:@"Halfling"])
    {
        self.Speed.text = @"25 Feet";
    }
    else
        self.Speed.text = @"30 Feet";
    
    if([self.Wp1NameType.text isEqual:@"%Ranged"])
    {
        self.Wp1Att.text = [lvc.character objectForKey:@"DEXmod"];
    }
    else
        self.Wp1Att.text = [lvc.character objectForKey:@"STRmod"];
    
    if([self.Wp2NameType.text isEqual:@"%Ranged"])
    {
        self.Wp2Att.text = [lvc.character objectForKey:@"DEXmod"];
    }
    else
        self.Wp2Att.text = [lvc.character objectForKey:@"STRmod"];
    
    if([self.Wp3NameType.text isEqual:@"%Ranged"])
    {
        self.Wp3Att.text = [lvc.character objectForKey:@"DEXmod"];
    }
    else
        self.Wp3Att.text = [lvc.character objectForKey:@"STRmod"];
    
    //Charisma Mod for Spellcasters
    if([Class isEqual:@"Bard"] || [Class isEqual:@"Sorcerer"] || [Class isEqual:@"Warlock"])
    {
        attack = prof + cha;
        DC = prof + cha + 8;
        self.SpellAtt.text = [NSString stringWithFormat:@"%d", attack];
        self.SpellDC.text = [NSString stringWithFormat:@"%d", DC];
    }
    
    //Wisdom Mod for Spellcasters
    else if([Class isEqual:@"Cleric"] || [Class isEqual:@"Ranger"])
    {
        attack = prof + wis;
        DC = prof + wis + 8;
        self.SpellAtt.text = [NSString stringWithFormat:@"%d", attack];
        self.SpellDC.text = [NSString stringWithFormat:@"%d", DC];

    }
    
    //Intelligence Mod for Spellcasters
    else if([Class isEqual:@"Paladin"] || [Class isEqual:@"Eldritch Knight"] || [Class isEqual:@"Arcane Trickster"] || [Class isEqual:@"Wizard"])
    {
        attack = prof + intel;
        DC = prof + intel + 8;
        self.SpellAtt.text = [NSString stringWithFormat:@"%d", attack];
        self.SpellDC.text = [NSString stringWithFormat:@"%d", DC];

    }
    
    else
    {
        attack = 0;
        DC = 0;
        self.SpellAtt.text = [NSString stringWithFormat:@"%d", attack];
        self.SpellDC.text = [NSString stringWithFormat:@"%d", DC];
    }
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)WP1NameEditEnd:(UITextField *)sender {
}
- (IBAction)WP1DamEditEnd:(UITextField *)sender {
}
- (IBAction)WP2NameEditEnd:(UITextField *)sender {
}
- (IBAction)WP2DamEditEnd:(UITextField *)sender {
}
- (IBAction)WP3NameEditEnd:(UITextField *)sender {
}
- (IBAction)WP3DamEditEnd:(UITextField *)sender {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
