//
//  BackgroundVC.m
//  FinalProject
//
//  Created by Trevor Eckhardt on 7/28/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "BackgroundVC.h"
#import <Parse/Parse.h>
#import "LoginVC.h"

@interface BackgroundVC ()
@property (weak, nonatomic) IBOutlet UITextView *Backstory;
@property (weak, nonatomic) IBOutlet UITextView *Flaws;
@property (weak, nonatomic) IBOutlet UITextView *Traits;
@property (weak, nonatomic) IBOutlet UITextView *Bonds;
@property (weak, nonatomic) IBOutlet UITextView *Ideals;

@end

@implementation BackgroundVC

LoginVC* lvc;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor brownColor];
    
    //Same issue here. Only see the background.
//    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Scroll2.png"]];
//    [self.view addSubview:background];
//    
//    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"Character" ofType:@"plist"];
//    NSArray* array = [[NSArray alloc] initWithContentsOfFile:filePath];
//    
//    NSMutableDictionary* CharInform = [array objectAtIndex:4];
    
    NSError *error = nil;
    
    self.Backstory.text = [lvc.character objectForKey:@"Backstory"];
    self.Flaws.text = [lvc.character objectForKey:@"Flaws"];
    self.Traits.text = [lvc.character objectForKey:@"Traits"];
    self.Bonds.text = [lvc.character objectForKey:@"Bonds"];
    self.Ideals.text = [lvc.character objectForKey:@"Ideals"];
    
    NSString *back = self.Backstory.text;
    NSString* flaw = self.Flaws.text;
    NSString* trait = self.Traits.text;
    NSString* bond = self.Bonds.text;
    NSString* idea = self.Ideals.text;
    
    //Is this the correct way of changing the values for my plist? Not sure.
    if (![back writeToFile:[lvc.character objectForKey:@"Backstory"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![flaw writeToFile:[lvc.character objectForKey:@"Flaws"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![trait writeToFile:[lvc.character objectForKey:@"Traits"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![bond writeToFile:[lvc.character objectForKey:@"Bonds"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }

    if (![idea writeToFile:[lvc.character objectForKey:@"Ideals"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
