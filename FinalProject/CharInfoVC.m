//
//  FirstViewController.m
//  FinalProject
//
//  Created by Trevor Eckhardt on 7/28/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

//App crashes when it tries to load this view.

#import "CharInfoVC.h"
#import <Parse/Parse.h>
#import "LoginVC.h"

@interface CharInfoVC ()

@property (weak, nonatomic) IBOutlet UILabel *STRmod;
@property (weak, nonatomic) IBOutlet UILabel *DEXmod;
@property (weak, nonatomic) IBOutlet UILabel *CONmod;
@property (weak, nonatomic) IBOutlet UILabel *INTmod;
@property (weak, nonatomic) IBOutlet UILabel *WISmod;
@property (weak, nonatomic) IBOutlet UILabel *CHAmod;
@property (weak, nonatomic) IBOutlet UITextField *CharName;
@property (weak, nonatomic) IBOutlet UITextField *CharClass;
@property (weak, nonatomic) IBOutlet UITextField *CharLvl;
@property (weak, nonatomic) IBOutlet UITextField *CharAlign;
@property (weak, nonatomic) IBOutlet UITextField *CharXP;
@property (weak, nonatomic) IBOutlet UITextField *CharHeight;
@property (weak, nonatomic) IBOutlet UITextField *CharWeight;
@property (weak, nonatomic) IBOutlet UITextField *CharAge;
@property (weak, nonatomic) IBOutlet UITextField *CharHairColor;
@property (weak, nonatomic) IBOutlet UITextField *CharEyeColor;
@property (weak, nonatomic) IBOutlet UITextField *CharSkin;
@property (weak, nonatomic) IBOutlet UITextField *CharRace;
@property (weak, nonatomic) IBOutlet UITextField *STR;
@property (weak, nonatomic) IBOutlet UITextField *DEX;
@property (weak, nonatomic) IBOutlet UITextField *CON;
@property (weak, nonatomic) IBOutlet UITextField *INT;
@property (weak, nonatomic) IBOutlet UITextField *WIS;
@property (weak, nonatomic) IBOutlet UITextField *CHA;

@end

@implementation CharInfoVC
//NSMutableDictionary* CharInform;
//NSMutableDictionary* Stats;
LoginVC* lvc;

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    //Issue here. It's not getting the right information, no matter whether they are Dictionaries or arrays.
//    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"Character" ofType:@"plist"];
//    NSArray* array = [[NSArray alloc] initWithContentsOfFile:filePath];
//    
//    CharInform = [array objectAtIndex:4];
//    Stats = [[array objectAtIndex:5] mutableCopy];
    
    //set Character information
    self.CharName.text = [lvc.character objectForKey:@"Name"];
    self.CharLvl.text = [lvc.character objectForKey:@"Level"];
    self.CharXP.text = [lvc.character objectForKey:@"XP"];
    self.CharWeight.text = [lvc.character objectForKey:@"Weight"];
    self.CharSkin.text = [lvc.character objectForKey:@"SkinColor"];
    self.CharRace.text = [lvc.character objectForKey:@"Race"];
    self.CharHeight.text = [lvc.character objectForKey:@"Height"];
    self.CharHairColor.text = [lvc.character objectForKey:@"HairColor"];
    self.CharEyeColor.text = [lvc.character objectForKey:@"EyeColor"];
    self.CharClass.text = [lvc.character objectForKey:@"Class"];
    self.CharAlign.text = [lvc.character objectForKey:@"Alignment"];
    self.CharAge.text = [lvc.character objectForKey:@"Age"];
    
    //set Stats
    int str, dex, con, wis, intel, cha;
    str = [lvc.character objectForKey:@"STR"];
    dex = [lvc.character objectForKey:@"DEX"];
    con = [lvc.character objectForKey:@"CON"];
    wis = [lvc.character objectForKey:@"WIS"];
    intel = [lvc.character objectForKey:@"INT"];
    cha = [lvc.character objectForKey:@"CHA"];
    
    self.STR.text = [NSString stringWithFormat:@"%d", str];
    self.DEX.text = [NSString stringWithFormat:@"%d", dex];
    self.CON.text = [NSString stringWithFormat:@"%d", con];
    self.INT.text = [NSString stringWithFormat:@"%d", intel];
    self.WIS.text = [NSString stringWithFormat:@"%d", wis];
    self.CHA.text = [NSString stringWithFormat:@"%d", cha];
    
    //set stat modifiers
    if([[self.STR text] integerValue] % 2 == 1)
    {
        int STRmodifier = ([[self.STR text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:STRmodifier] forKey:@"STRmod"];
        self.STRmod.text = [@(STRmodifier) stringValue];
    }
    else
    {
        int STRmodifier = ([[self.STR text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:STRmodifier] forKey:@"STRmod"];
        self.STRmod.text = [@(STRmodifier) stringValue];
    }
    
    if([[self.DEX text] integerValue] % 2 == 1)
    {
        int DEXmodifier = ([[self.DEX text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:DEXmodifier] forKey:@"DEXmod"];
        self.DEXmod.text = [@(DEXmodifier) stringValue];
    }
    else
    {
        int DEXmodifier = ([[self.DEX text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:DEXmodifier] forKey:@"DEXmod"];
        self.DEXmod.text = [@(DEXmodifier) stringValue];
    }
    
    if([[self.CON text] integerValue] % 2 == 1)
    {
        int CONmodifier = ([[self.CON text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:CONmodifier] forKey:@"CONmod"];
        self.CONmod.text = [@(CONmodifier) stringValue];
    }
    else
    {
        int CONmodifier = ([[self.CON text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:CONmodifier] forKey:@"CONmod"];
        self.CONmod.text = [@(CONmodifier) stringValue];
    }
    
    if([[self.INT text] integerValue] % 2 == 1)
    {
        int INTmodifier = ([[self.INT text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:INTmodifier] forKey:@"INTmod"];
        self.INTmod.text = [@(INTmodifier) stringValue];
    }
    else
    {
        int INTmodifier = ([[self.INT text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:INTmodifier] forKey:@"INTmod"];
        self.INTmod.text = [@(INTmodifier) stringValue];
    }
    
    if([[self.WIS text] integerValue] % 2 == 1)
    {
        int WISmodifier = ([[self.WIS text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:WISmodifier] forKey:@"WISmod"];
        self.CONmod.text = [@(WISmodifier) stringValue];
    }
    else
    {
        int WISmodifier = ([[self.WIS text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:WISmodifier] forKey:@"WISmod"];
        self.WISmod.text = [@(WISmodifier) stringValue];
    }
    
    if([[self.CHA text] integerValue] % 2 == 1)
    {
        int CHAmodifier = ([[self.CHA text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:CHAmodifier] forKey:@"CHAmod"];
        self.CHAmod.text = [@(CHAmodifier) stringValue];
    }
    else
    {
        int CHAmodifier = ([[self.CHA text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:CHAmodifier] forKey:@"CHAmod"];
        self.CHAmod.text = [@(CHAmodifier) stringValue];
    }
    
    //set Proficiency bonus
    NSString* string = [lvc.character objectForKey:@"Level"];
    int prof = 1 + (([string intValue] + 4 - 1) / 4);
    [lvc.character setObject:[NSNumber numberWithInt:prof] forKey:@"ProfBonus"];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Scroll2.png"]];
    [self.view addSubview:background];
}

//
// Not sure if I'm doing these right. Help would be greatly appreciated. I'm trying to make the plist update after the user changes the value of the specific field.
//
- (IBAction)NameEditEnd:(UITextField *)sender
{
    [lvc.character setObject:sender forKey:@"Name"];
}
- (IBAction)ClassEditEnd:(UITextField *)sender
{
    [lvc.character setObject:sender forKey:@"Class"];
}
- (IBAction)LevelEditEnd:(UITextField *)sender
{
    [lvc.character setObject:sender forKey:@"Level"];
}
- (IBAction)XPEditEnd:(UITextField *)sender
{
    [lvc.character setObject:sender forKey:@"XP"];
}
- (IBAction)HeightEditEnd:(UITextField *)sender
{
    [lvc.character setObject:sender forKey:@"Height"];
}
- (IBAction)WeightEditEnd:(UITextField *)sender
{
    [lvc.character setObject:sender forKey:@"Weight"];
}
- (IBAction)AgeEditEnd:(UITextField *)sender
{
    [lvc.character setObject:sender forKey:@"Age"];
}
- (IBAction)RaceEditEnd:(UITextField *)sender {
    [lvc.character setObject:sender forKey:@"Race"];
}
- (IBAction)SkinEditEnd:(UITextField *)sender {
    [lvc.character setObject:sender forKey:@"SkinColor"];
}
- (IBAction)EyeEditEnd:(UITextField *)sender {
    [lvc.character setObject:sender forKey:@"EyeColor"];
}
- (IBAction)HairEditEnd:(UITextField *)sender {
    [lvc.character setObject:sender forKey:@"HairColor"];
}
- (IBAction)STREditEnd:(UITextField *)sender {
    [lvc.character setObject:sender forKey:@"STR"];
    
    if([[self.STR text] integerValue] % 2 == 1)
    {
        int STRmodifier = ([[self.STR text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:STRmodifier] forKey:@"STRmod"];
        self.STRmod.text = [@(STRmodifier) stringValue];
    }
    else
    {
        int STRmodifier = ([[self.STR text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:STRmodifier] forKey:@"STRmod"];
        self.STRmod.text = [@(STRmodifier) stringValue];
    }
}
- (IBAction)DEXEditEnd:(UITextField *)sender {
    [lvc.character setObject:sender forKey:@"DEX"];
    
    if([[self.DEX text] integerValue] % 2 == 1)
    {
        int DEXmodifier = ([[self.DEX text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:DEXmodifier] forKey:@"DEXmod"];
        self.DEXmod.text = [@(DEXmodifier) stringValue];
    }
    else
    {
        int DEXmodifier = ([[self.DEX text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:DEXmodifier] forKey:@"DEXmod"];
        self.DEXmod.text = [@(DEXmodifier) stringValue];
    }

}
- (IBAction)CONEditEnd:(UITextField *)sender {
    [lvc.character setObject:sender forKey:@"CON"];
    
    if([[self.CON text] integerValue] % 2 == 1)
    {
        int CONmodifier = ([[self.CON text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:CONmodifier] forKey:@"CONmod"];
        self.CONmod.text = [@(CONmodifier) stringValue];
    }
    else
    {
        int CONmodifier = ([[self.CON text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:CONmodifier] forKey:@"CONmod"];
        self.CONmod.text = [@(CONmodifier) stringValue];
    }
}
- (IBAction)INTEditEnd:(UITextField *)sender {
    [lvc.character setObject:sender forKey:@"INT"];
    
    
    if([[self.INT text] integerValue] % 2 == 1)
    {
        int INTmodifier = ([[self.INT text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:INTmodifier] forKey:@"INTmod"];
        self.INTmod.text = [@(INTmodifier) stringValue];
    }
    else
    {
        int INTmodifier = ([[self.INT text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:INTmodifier] forKey:@"INTmod"];
        self.INTmod.text = [@(INTmodifier) stringValue];
    }
}
- (IBAction)WISEditEnd:(UITextField *)sender {
    [lvc.character setObject:sender forKey:@"WIS"];
    
    if([[self.WIS text] integerValue] % 2 == 1)
    {
        int WISmodifier = ([[self.WIS text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:WISmodifier] forKey:@"WISmod"];
        self.CONmod.text = [@(WISmodifier) stringValue];
    }
    else
    {
        int WISmodifier = ([[self.WIS text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:WISmodifier] forKey:@"WISmod"];
        self.WISmod.text = [@(WISmodifier) stringValue];
    }
}
- (IBAction)CHAEditEnd:(UITextField *)sender {
    [lvc.character setObject:sender forKey:@"CHA"];
    
    if([[self.CHA text] integerValue] % 2 == 1)
    {
        int CHAmodifier = ([[self.CHA text] integerValue] - 11) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:CHAmodifier] forKey:@"CHAmod"];
        self.CHAmod.text = [@(CHAmodifier) stringValue];
    }
    else
    {
        int CHAmodifier = ([[self.CHA text] integerValue] - 10) / 2.0;
        [lvc.character setObject:[NSNumber numberWithInt:CHAmodifier] forKey:@"CHAmod"];
        self.CHAmod.text = [@(CHAmodifier) stringValue];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
