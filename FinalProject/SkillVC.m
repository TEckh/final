//
//  SkillVC.m
//  FinalProject
//
//  Created by Trevor Eckhardt on 7/28/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "SkillVC.h"
#import <Parse/Parse.h>
#import "LoginVC.h"

@interface SkillVC ()
@property (weak, nonatomic) IBOutlet UILabel *ProfBonus;
@property (weak, nonatomic) IBOutlet UILabel *Inspir;
@property (weak, nonatomic) IBOutlet UILabel *PassPerc;
@property (weak, nonatomic) IBOutlet UILabel *Acro;
@property (weak, nonatomic) IBOutlet UILabel *AnimHand;
@property (weak, nonatomic) IBOutlet UILabel *Arcana;
@property (weak, nonatomic) IBOutlet UILabel *Athl;
@property (weak, nonatomic) IBOutlet UILabel *Decep;
@property (weak, nonatomic) IBOutlet UILabel *Hist;
@property (weak, nonatomic) IBOutlet UILabel *Insight;
@property (weak, nonatomic) IBOutlet UILabel *Intim;
@property (weak, nonatomic) IBOutlet UILabel *Invest;
@property (weak, nonatomic) IBOutlet UILabel *Medi;
@property (weak, nonatomic) IBOutlet UILabel *Nature;
@property (weak, nonatomic) IBOutlet UILabel *Perc;
@property (weak, nonatomic) IBOutlet UILabel *Perform;
@property (weak, nonatomic) IBOutlet UILabel *Persuasion;
@property (weak, nonatomic) IBOutlet UILabel *Relig;
@property (weak, nonatomic) IBOutlet UILabel *Sleight;
@property (weak, nonatomic) IBOutlet UILabel *Stealth;
@property (weak, nonatomic) IBOutlet UILabel *Surv;

@end

@implementation SkillVC

int str, dex, con, wis, intel, cha, prof;
LoginVC* lvc;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor brownColor];
    
    //Same issue as with the previous screens.
//    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Scroll2.png"]];
//    [self.view addSubview:background];

    
//    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"Character" ofType:@"plist"];
//    NSArray* array = [[NSArray alloc] initWithContentsOfFile:filePath];
//    
//    NSMutableDictionary* Stats = [array objectAtIndex:5];
    
    prof = [[lvc.character objectForKey:@"ProfBonus"] integerValue];
    str = [[lvc.character objectForKey:@"STRmod"] integerValue];
    dex = [[lvc.character objectForKey:@"DEXmod"] integerValue];
    con = [[lvc.character objectForKey:@"CONmod"] integerValue];
    intel = [[lvc.character objectForKey:@"INTmod"] integerValue];
    wis = [[lvc.character objectForKey:@"WISmod"] integerValue];
    cha = [[lvc.character objectForKey:@"CHAmod"] integerValue];
    
    self.PassPerc.text = [@(wis + 10) stringValue];
    self.ProfBonus.text = [@(prof) stringValue];
    
    // Do any additional setup after loading the view.
}

- (IBAction)InspirTouch:(id)sender
{
    if([self.Inspir.text isEqual:@"Yes"])
    {
        self.Inspir.text = @"No";
    }
    else
        self.Inspir.text = @"Yes";
        
}
- (IBAction)SurvTouch:(id)sender
{
    if([self.Surv.text integerValue] > wis)
    {
        self.Surv.text = [@(wis) stringValue];
    }
    else
        self.Surv.text = [@(wis + prof) stringValue];
}
- (IBAction)StealthTouch:(id)sender
{
    if([self.Stealth.text integerValue] > dex)
    {
        self.Stealth.text = [@(dex) stringValue];
    }
    else
        self.Stealth.text = [@(dex + prof) stringValue];
}
- (IBAction)SleightTouch:(id)sender
{
    if([self.Sleight.text integerValue] > dex)
    {
        self.Sleight.text = [@(dex) stringValue];
    }
    else
        self.Sleight.text = [@(dex + prof) stringValue];
}
- (IBAction)RelTouch:(id)sender
{
    if([self.Relig.text integerValue] > intel)
    {
        self.Relig.text = [@(intel) stringValue];
    }
    else
        self.Relig.text = [@(intel + prof) stringValue];
}
- (IBAction)PersuadTouch:(id)sender
{
    if([self.Persuasion.text integerValue] > cha)
    {
        self.Persuasion.text = [@(cha) stringValue];
    }
    else
        self.Persuasion.text = [@(cha + prof) stringValue];
}
- (IBAction)PerfTouch:(id)sender
{
    if([self.Perform.text integerValue] > cha)
    {
        self.Perform.text = [@(cha) stringValue];
    }
    else
        self.Perform.text = [@(cha + prof) stringValue];
}
- (IBAction)PercTouch:(UIButton *)sender
{
    if([self.Perc.text integerValue] > wis)
    {
        self.Perc.text = [@(wis) stringValue];
    }
    else
        self.Perc.text = [@(wis + prof) stringValue];
}
- (IBAction)NatTouch:(UIButton *)sender
{
    if([self.Nature.text integerValue] > intel)
    {
        self.Nature.text = [@(intel) stringValue];
    }
    else
        self.Nature.text = [@(intel + prof) stringValue];
}
- (IBAction)MedTouch:(UIButton *)sender
{
    if([self.Medi.text integerValue] > wis)
    {
        self.Medi.text = [@(wis) stringValue];
    }
    else
        self.Medi.text = [@(wis + prof) stringValue];
}
- (IBAction)AcroTouch:(UIButton *)sender
{
    if([self.Acro.text integerValue] > dex)
    {
        self.Acro.text = [@(dex) stringValue];
    }
    else
        self.Acro.text = [@(dex + prof) stringValue];
}
- (IBAction)AnimTouch:(UIButton *)sender
{
    if([self.AnimHand.text integerValue] > wis)
    {
        self.AnimHand.text = [@(wis) stringValue];
    }
    else
        self.AnimHand.text = [@(wis + prof) stringValue];
}
- (IBAction)ArcaneTouch:(UIButton *)sender
{
    if([self.Arcana.text integerValue] > intel)
    {
        self.Arcana.text = [@(intel) stringValue];
    }
    else
        self.Arcana.text = [@(intel + prof) stringValue];
}
- (IBAction)AthletTouch:(UIButton *)sender
{
    if([self.Athl.text integerValue] > str)
    {
        self.Athl.text = [@(str) stringValue];
    }
    else
        self.Athl.text = [@(str + prof) stringValue];
}
- (IBAction)DecepTouch:(UIButton *)sender
{
    if([self.Decep.text integerValue] > cha)
    {
        self.Decep.text = [@(cha) stringValue];
    }
    else
        self.Decep.text = [@(cha + prof) stringValue];
}
- (IBAction)HistTouch:(UIButton *)sender
{
    if([self.Hist.text integerValue] > intel)
    {
        self.Hist.text = [@(intel) stringValue];
    }
    else
        self.Hist.text = [@(intel + prof) stringValue];
}
- (IBAction)IntimTouch:(UIButton *)sender
{
    if([self.Intim.text integerValue] > cha)
    {
        self.Intim.text = [@(cha) stringValue];
    }
    else
        self.Intim.text = [@(cha + prof) stringValue];
}
- (IBAction)InvestTouch:(UIButton *)sender
{
    if([self.Invest.text integerValue] > intel)
    {
        self.Invest.text = [@(intel) stringValue];
    }
    else
        self.Invest.text = [@(intel + prof) stringValue];
}
- (IBAction)InsightTouch:(UIButton *)sender
{
    if([self.Insight.text integerValue] > wis)
    {
        self.Insight.text = [@(wis) stringValue];
    }
    else
        self.Insight.text = [@(wis + prof) stringValue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
