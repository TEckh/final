//
//  SpellVC.m
//  FinalProject
//
//  Created by Trevor Eckhardt on 7/28/15.
//  Copyright (c) 2015 Trevor Eckhardt. All rights reserved.
//

#import "SpellVC.h"
#import <Parse/Parse.h>
#import "LoginVC.h"

@interface SpellVC ()
@property (weak, nonatomic) IBOutlet UITextView *LVL0list;
@property (weak, nonatomic) IBOutlet UITextView *LVL1list;
@property (weak, nonatomic) IBOutlet UITextView *LVL2list;
@property (weak, nonatomic) IBOutlet UITextView *LVL3list;
@property (weak, nonatomic) IBOutlet UITextView *LVL4list;
@property (weak, nonatomic) IBOutlet UITextView *LVL5list;
@property (weak, nonatomic) IBOutlet UITextView *LVL6list;
@property (weak, nonatomic) IBOutlet UITextView *LVL7list;
@property (weak, nonatomic) IBOutlet UITextView *LVL8list;
@property (weak, nonatomic) IBOutlet UITextView *LVL9list;


@end

@implementation SpellVC

LoginVC* lvc;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor brownColor];
    
    //Pretty much the same issue as with the rest of them.
//    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Scroll2.png"]];
//    [self.view addSubview:background];
    
//    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"Character" ofType:@"plist"];
//    NSArray* array = [[NSArray alloc] initWithContentsOfFile:filePath];
//    
//    NSMutableDictionary* Spells = [array objectAtIndex:2];
    
    //set spell lists
    self.LVL0list.text = [lvc.character objectForKey:@"SpellLvl0"];
    self.LVL1list.text = [lvc.character objectForKey:@"SpellLvl1"];
    self.LVL2list.text = [lvc.character objectForKey:@"SpellLvl2"];
    self.LVL3list.text = [lvc.character objectForKey:@"SpellLvl3"];
    self.LVL4list.text = [lvc.character objectForKey:@"SpellLvl4"];
    self.LVL5list.text = [lvc.character objectForKey:@"SpellLvl5"];
    self.LVL6list.text = [lvc.character objectForKey:@"SpellLvl6"];
    self.LVL7list.text = [lvc.character objectForKey:@"SpellLvl7"];
    self.LVL8list.text = [lvc.character objectForKey:@"SpellLvl8"];
    self.LVL9list.text = [lvc.character objectForKey:@"SpellLvl9"];
    
    NSError* error = nil;
    NSString* lvl0 = self.LVL0list.text;
    NSString* lvl1 = self.LVL1list.text;
    NSString* lvl2 = self.LVL1list.text;
    NSString* lvl3 = self.LVL1list.text;
    NSString* lvl4 = self.LVL1list.text;
    NSString* lvl5 = self.LVL1list.text;
    NSString* lvl6 = self.LVL1list.text;
    NSString* lvl7 = self.LVL1list.text;
    NSString* lvl8 = self.LVL1list.text;
    NSString* lvl9 = self.LVL1list.text;
    
    if (![lvl0 writeToFile:[lvc.character objectForKey:@"Lvl0"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![lvl1 writeToFile:[lvc.character objectForKey:@"Lvl1"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![lvl2 writeToFile:[lvc.character objectForKey:@"Lvl2"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![lvl3 writeToFile:[lvc.character objectForKey:@"Lvl3"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![lvl4 writeToFile:[lvc.character objectForKey:@"Lvl4"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![lvl5 writeToFile:[lvc.character objectForKey:@"Lvl5"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![lvl6 writeToFile:[lvc.character objectForKey:@"Lvl6"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![lvl7 writeToFile:[lvc.character objectForKey:@"Lvl7"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![lvl8 writeToFile:[lvc.character objectForKey:@"Lvl8"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    if (![lvl9 writeToFile:[lvc.character objectForKey:@"Lvl9"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        NSLog(@"Error saving - %@", error);
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
